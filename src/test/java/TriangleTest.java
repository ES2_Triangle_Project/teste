import org.junit.jupiter.api.*;
import trianglepackage.Triangle;

public class TriangleTest {
    private Triangle triangle;

    @Test
    public void testIsImpossible_TC2(){
        triangle = new Triangle(1000,10,10);
        Assertions.assertEquals("scalene",
                triangle.classify(), "Scalene");
    }
    @Test
    public void testIsequilateral_TC1(){
        triangle = new Triangle(3,3,3);

        Assertions.assertFalse(triangle.isImpossible());

        Assertions.assertEquals("equilateral",
                triangle.classify(), "Triangle should be equilateral");

    }

    @Test
    public void test0ValueInputs_TC3(){
        triangle = new Triangle(0,0,0);

        Assertions.assertEquals("impossible",
                triangle.classify(), "Triangle is not possible");

    }

    @Test
    public void testAllInputsMaxInt_TC4(){
        triangle = new Triangle(Integer.MAX_VALUE,Integer.MAX_VALUE,Integer.MAX_VALUE);

        Assertions.assertEquals("equilateral",
                triangle.classify(), "Triangle should be equilateral");

    }

}
