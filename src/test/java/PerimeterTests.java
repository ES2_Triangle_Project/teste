import org.junit.jupiter.api.*;
import trianglepackage.Triangle;
public class PerimeterTests {
    private Triangle triangle;
    @Test
    public void TriangleIsImpossible(){
        triangle = new Triangle(1,10,12);
        Assertions.assertEquals(-1,
                triangle.getPerimeter(), "should be impossible");
    }
}
